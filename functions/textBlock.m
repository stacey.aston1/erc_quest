function [keyCode,buttons] = textBlock(params,str)

% Show question and instructions
for eye = {'left','right'}

    if strcmp(eye,'left')
        % Select left-eye image buffer for drawing (buffer = 0)
        Screen('SelectStereoDrawBuffer', params.window, 0);
    else
        % Select right-eye image buffer for drawing (buffer = 1)
        Screen('SelectStereoDrawBuffer', params.window, 1);
    end

    Screen('FillRect', params.window, params.backColour);
    DrawFormattedText(params.window, str,...
        'center','center',...
        params.textColour);

end
Screen('Flip', params.window);

% wait for a response
[~,~,keyCode] = KbCheck;
[~,~,~,buttons] = WinJoystickMex(0);
while ~params.isContinue(buttons) && ~params.isQuit(keyCode)
    [~,~,keyCode] = KbCheck;
    [~,~,~,buttons] = WinJoystickMex(0);
end

% make sure button is realeased before continuing
while params.isContinue(buttons) && ~params.isQuit(keyCode)
    [~,~,keyCode] = KbCheck;
    [~,~,~,buttons] = WinJoystickMex(0);
end

end