function data = setUpAssesmentStimuli()

% only the disparity cue is present during assesment
data.disparityPresent = 1;
data.sizePresent = 0;
data.audioPresent = 0;

% set up trials
depths = [ones(10,1)*133;ones(5,1)*138;ones(10,1)*143];
data.depths = depths(randperm(length(depths)));
data.responses = cell(length(depths),1);

end





