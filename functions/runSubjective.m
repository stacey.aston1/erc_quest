function keyCode = runSubjective(params,part,condition)

% the JND differences
diffInJND = [2,1];

% the instructions
instructions = cell(2,1);
instructions{1,1} = ['Well done! You have completed this block of trials. \n\n\n',...
    'We will now present another pair of objects. As before, see if you \n\n',...
    'can tell which object is further away. \n\n\n',...
    'Press Y to continue.'];
instructions{2,1} = ['Thank you! \n\n\n',...
    'We will now present one last pair of objects. Again, \n\n',...
    'see if you can tell which object is further away. \n\n\n',...
    'Press Y to continue.'];

% the questions
question = cell(2,1);
question{1,1} = ['How strongly did you feel that the objects were at \n\n',...
    'different distances?'];
question{2,1} = ['How hard did you feel that you had to concentrate to \n\n',...
    'tell which object was further away?'];

% the labels
labels = cell(2,3);
labels(1,:) = {'Not at all','Somewhat','Extremely strongly'};
labels(2,:) = {'Not at all','Somewhat','Extremely hard'};

% convert the JND difference to real differences for this condition
Tab = readtable([params.parentFolder,'data\',part.pID,'\',part.pID,'_',...
    params.taskType,'_',condition,'_measures.csv']);
diffs = diffInJND.*Tab{1,'JND'};
comparisons = zeros(2,2);
comparisons(1,:) = diffs/2;
comparisons(2,:) = -diffs/2;

% find out which cues to display in this block
if strcmp(condition,'S')
    disparity = 0;
    size = 1;
    audio = 0;
elseif strcmp(condition,'D')
    disparity = 1;
    size = 0;
    audio = 0;
elseif strcmp(condition,'A')
    disparity = 0;
    size = 0;
    audio = 1;
elseif ismember(condition,'SD')
    disparity = 1;
    size = 1;
    audio = 0;
elseif ismember(condition,'AD')
    disparity = 1;
    size = 0;
    audio = 1;
end

response = nan(2,1);

for q = 1:2
    
    % ----- GET THE TWO DEPTHS -----
    
    % get the depths and randomly asign to comparison 1 or 2
    if rand > 0.5
        c1Depth = comparisons(q,1)*params.q.halfRange + params.q.reference;
        c2Depth = comparisons(q,2)*params.q.halfRange + params.q.reference;
    else
        c2Depth = comparisons(q,1)*params.q.halfRange + params.q.reference;
        c1Depth = comparisons(q,2)*params.q.halfRange + params.q.reference;
    end
    
    % ----- SHOW INSTRUCTIONS -----
    
    if q == 1
        textBlock(params,instructions{1,1});
    else
        textBlock(params,instructions{2,1});
    end
    
    % ----- SHOW THE STIMULI -----
    
    % set the square colours for this trial
    squareColours = params.squareColours(randi(3,[length(params.squareXcenters),1]),:)';
    
    % show each comparison
    allowResponse = 0;
    keyCode = showStimulus2IFC(params,disparity,size,audio,...
        c1Depth,c1Depth,c1Depth,part.interocularDistance,...
        allowResponse,squareColours,part.mapDir);
    % exit here is quit key was hit
    if params.isQuit(keyCode)
        break
    end
    keyCode = showStimulus2IFC(params,disparity,size,audio,...
        c2Depth,c2Depth,c2Depth,part.interocularDistance,...
        allowResponse,squareColours,part.mapDir);
    
    % ----- GET A RESPONSE -----
    
    if ~params.isQuit(keyCode)
        [keyCode,response(q,1)] = getSubjectiveResponse(params,squareColours,...
            question{q,1},labels);
    end
    
end

% ----- SAVE THESE RESPONSES -----

Tab = table(question,response);
writetable(Tab,[params.parentFolder,'data\',part.pID,'\',part.pID,'_',...
    params.taskType,'_',condition,'_subjective.csv']);

end








