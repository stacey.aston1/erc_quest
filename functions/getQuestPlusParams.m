function q = getQuestPlusParams(maxTrials)

% the model
q.F = @(x,mu,sigma,gamma,lambda) gamma + (1 - gamma - lambda) .* normcdf(x,mu,sigma);

% parameter domains
q.mu = -0.1:0.025:0.1;
q.sigma = 0.01:0.01:1.25;
q.gamma = 0;
q.lambda = 0.01:0.01:0.15;
q.paramDomain = {q.mu, q.sigma, q.gamma, q.lambda};

% stimulus domain
q.stimDomain = -1:0.1:1;

% response domain
q.respDomain = [0 1]; % closer/further

% set stopping rule and criterion
q.stopRule = 'ntrials';
q.stopCriterion = maxTrials;

end