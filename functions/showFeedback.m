function keyCode = showFeedback(params,feedback)

for eye = {'left','right'}
    
    if strcmp(eye,'left')
        % Select left-eye image buffer for drawing (buffer = 0)
        Screen('SelectStereoDrawBuffer', params.window, 0);
    else
        % Select right-eye image buffer for drawing (buffer = 1)
        Screen('SelectStereoDrawBuffer', params.window, 1);
    end
    
    Screen('FillRect', params.window, params.backColour);
    DrawFormattedText(params.window, feedback,...
        'center','center',...
        params.textColour);
    Screen('Flip', params.window);
    
end

tic;
[~,~,keyCode] = KbCheck;
while ~params.isQuit(keyCode) && toc < params.feedbackDuration
    [~,~,keyCode] = KbCheck;
end

end
















