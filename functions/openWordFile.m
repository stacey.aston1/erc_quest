function [] = openWordFile(params,part)

% Initialization
word = actxserver('Word.Application');      
word.Visible =1;                           
document=word.Documents.Add;
selection=word.Selection; 
selection.Font.Name='Ariel';                
selection.Font.Size=14;                     

% Questions                        
selection.TypeText(['Q1: Please tell us, in much detail as you can, how you ',...
    'did the task today. What did you do, or pay attention to?']);
selection.TypeParagraph;
selection.TypeText(['Please enter your response below. Feel free to ',...
    'move the keyboard to a position that is comfortable for you. Let the ',...
    'experimenter know when you are finished.']);
selection.TypeParagraph;

if part.session == 4
    selection.TypeParagraph;
    selection.TypeParagraph;
    selection.TypeParagraph;
    selection.TypeText(['Q2: Do you feel that how you did the task changed ',...
        'over time, since you started the study?']);
    selection.TypeParagraph;
    selection.TypeText('Please enter your response below.');
    selection.TypeParagraph;
end

% save the file
document.SaveAs2([params.parentFolder,'data\',part.pID,'\',part.pID,...
    '_Q3_Day_',num2str(part.session),'.doc']);

end