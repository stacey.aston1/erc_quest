function [data,params,q] = setUpInterleavedStimuli(params,part)

% attached participant info to data structure
data.part = part;

% conditions
if strcmp(params.taskType,'combination')
    % randomly order reweighting blocks
    conditions = {'SD+-','AD+-'};
    conditions = conditions(randperm(length(conditions)));
    % save this order to be used again later
    save([params.parentFolder,'data\',part.pID,'_',...
        'reweighting_order.mat'],'conditions')
    data.conditions = conditions;
else % load the previous reweighting order
    try
        load([params.parentFolder,'data\',part.pID,'_',...
            'reweighting_order.mat'],'conditions')
    catch
        error('No noiseless reweighting data for this participant number.')
    end
    data.conditions = conditions;
end

% what cues are present in each condition?
data.disparityPresent = nan(1,length(conditions));
data.sizePresent = nan(1,length(conditions));
data.audioPresent = nan(1,length(conditions));
for c = 1:length(conditions)
    if strcmp(data.conditions{1,c},'SD+-')
        data.disparityPresent(1,c) = 1;
        data.sizePresent(1,c) = 1;
        data.audioPresent(1,c) = 0;
    elseif strcmp(data.conditions{1,c},'AD+-')
        data.disparityPresent(1,c) = 1;
        data.sizePresent(1,c) = 0;
        data.audioPresent(1,c) = 1;
    end
end

% get (normalised) QUEST+ parameters
q = getQuestPlusParams(params.maxTrials);

% non-normalised parameters
if strcmp(part.group,'F')
    q.reference = 133;
    q.halfRange = 4; % 129 to 137
elseif strcmp(part.group,'B')
    q.reference = 143;
    q.halfRange = 4; % 139 to 147
end

% define the reference waveform
params.reference = q.reference;
semitoneDiff = getSemitoneDifference(params.distance,params.reference,part.mapDir);
params.refWaveform = getWaveform(semitoneDiff,params);

end







