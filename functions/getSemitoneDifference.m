function semitoneDiff = getSemitoneDifference(distanceToScreen,depthToSimulate,mapDir)
% map the change in depth to a change in semitones

% semitone change per cm depth change
semitonesPerCM = 0.1*mapDir;

% get the change in distance (in cm)
deltaD = depthToSimulate - distanceToScreen;

% convert to a semitone change
semitoneDiff = semitonesPerCM*deltaD;

end

