function [data,params] = runStaircases(params,part)

% ----- SET UP TRIALS -----
[data,params,q] = setUpStimuli(params,part);
params.q = q;

% ----- EXPERIMENT LOOP -----

% initialise the condition counter
conditionCounter = 0;

% check if the quit key has been hit
[~,~,keyCode] = KbCheck;

% initialise easy trial tracker
easyTrial = 0;
easyCounter = 0;
easy_depths = nan(floor(q.stopCriterion/10)-1,1);
easy_responses = cell(floor(q.stopCriterion/10)-1,1);
easy_reaction_times = nan(floor(q.stopCriterion/10)-1,1);

% break message
breakMessage1 = 'Well done! You have finished block ';
breakMessage2 = ' more to go!';
breakMessage3 = 'Please take a break and press Y when you are ready to continue';

% run through each block (condition) unless the quit key is hit
while ~params.isQuit(keyCode) && conditionCounter < length(data.conditions)

    % ----- TIME TO TAKE A BREAK IF NOT FIRST CONDITION -----

    if conditionCounter
        if ismember(params.taskType,{'combination','noisy_combination'})
            totalConds = length(data.conditions) + 2;
        else
            totalConds = length(data.conditions);
        end
        currentCond = conditionCounter;
        leftConds = totalConds - currentCond;
        breakText = [breakMessage1,num2str(currentCond),' \n\n ',...
            num2str(leftConds),breakMessage2,' \n\n',...
            breakMessage3];
        textBlock(params,breakText);
    end
    
    % ----- COUNT UP THE CONDITION COUNTER AND SET UP THIS STAIRCASE -----
    
    % next condition
    conditionCounter = conditionCounter + 1;

    if ismember(data.conditions{1,conditionCounter},{'SDi','ADi'})
        % find out which cue is least reliable to flip sign of that one
        DTab = readtable([params.parentFolder,'data\',part.pID,'\',part.pID,'_',...
            params.taskType,'_D_measures.csv']);
        if strcmp(data.conditions{1,conditionCounter},'SDi')
            OTab = readtable([params.parentFolder,'data\',part.pID,'\',part.pID,'_',...
                params.taskType,'_S_measures.csv']);
        else
            OTab = readtable([params.parentFolder,'data\',part.pID,'\',part.pID,'_',...
                params.taskType,'_A_measures.csv']);
        end
        if DTab{1,'sensory_noise'} > OTab{1,'sensory_noise'}
            % invert disparity cue
            disparityInvert = 1;
        else
            % invert other cue
            disparityInvert = 0;
        end
    end

    % set up a new QUEST+ object
    QP = QuestPlus(q.F, q.stimDomain, q.paramDomain, q.respDomain, ...
        q.stopRule, q.stopCriterion);
    
    % initialise with uniform priors (possibly from saved file to save
    % time)
    q.priors = {ones(1,length(q.paramDomain{1}))/length(q.paramDomain{1}), ...
        ones(1,length(q.paramDomain{2}))/length(q.paramDomain{2}), ...
        ones(1,length(q.paramDomain{3}))/length(q.paramDomain{3}), ...
        ones(1,length(q.paramDomain{4}))/length(q.paramDomain{4})};
    q.fn = [params.parentFolder,'functions\QuestPlusLikelihoodsExp1.mat'];
    if exist(q.fn, 'file')
        QP.initialise(q.priors, q.fn);
    else
        QP.initialise(q.priors);
        QP.saveLikelihoods(q.fn);
    end
    
    % ----- PROCESS CUE INFORMATION -----
    
    % find out which cues to display in this block
    
    disparity = data.disparityPresent(1,conditionCounter);
    size = data.sizePresent(1,conditionCounter);
    audio = data.audioPresent(1,conditionCounter);
    
    % ----- RUN THIS BLOCK OF TRIALS -----
    
    % where to save responses
    responses = cell(QP.maxNTrials,1);
    
    % where to save RTs
    reaction_times = nan(QP.maxNTrials,1);
    
    % estimate parameters using QUEST+
    while ~QP.isFinished() && ~params.isQuit(keyCode)
        
        % check if we are due an easy trial
        if ~isempty(QP.history_stim) && ~easyTrial && ...
                ~mod(length(QP.history_stim),params.easyInt) && ~params.isQuit(keyCode)
            % time for a memory dump (just incase) and an easy trial
            save([params.parentFolder,'memoryDumps\temp',params.compID{1,1},'\',part.pID,'_',...
                params.taskType,'_',data.conditions{1,conditionCounter},...
                '_',datestr(now,'dd-mmm-yyyy-HH-MM-SS')])
            easyTrial = 1;
        elseif ~isempty(QP.history_stim) && ...
                ~mod(length(QP.history_stim),params.progressInt) && ~params.isQuit(keyCode)
            if ismember(params.taskType,{'combination','noisy_combination'})
                totalConds = length(data.conditions) + 2;
            else
                totalConds = length(data.conditions);
            end
            currentCond = conditionCounter;
            percent = round(length(QP.history_stim)/QP.maxNTrials*100);
            progText = ['Well done! You have completed ',num2str(percent),'% of trials in \n\n',...
                ' condition ',num2str(currentCond),' of ',num2str(totalConds),' \n\n\n',...
                'Press Y to continue'];
            textBlock(params,progText);
            easyTrial = 0;
        else
            easyTrial = 0;
        end

        % are we doing an easy trial or not?
        if ~easyTrial
            % get the reference depth
            refDepth = q.reference;
            % get next test stimulus
            test = QP.getTargetStim();
        else
            % get the reference depth
            refDepth = -0.5*q.halfRange + q.reference;
            % get the test depth
            test = 0.5;
        end

        if ismember(data.conditions{1,conditionCounter},{'SDi','ADi'})
            % figure out which cue to invert
            if disparityInvert
                disparityDepth = -test*q.halfRange + q.reference;
                otherDepth = test*q.halfRange + q.reference;
            else
                disparityDepth = test*q.halfRange + q.reference;
                otherDepth = -test*q.halfRange + q.reference;
            end
        else
            % convert to a depth to show
            disparityDepth = test*q.halfRange + q.reference;
            otherDepth = disparityDepth;
        end

        % ----- SHOW THE STIMULI -----
        
        % set the square colours for this trial
        squareColours = params.squareColours(randi(3,[length(params.squareXcenters),1]),:)';
        
        % randomly decide to show reference first or second
        refFirst = rand > 0.5;
        if refFirst
            % show the reference, without allowing a response
            allowResponse = 0;
            [keyCode,~,~] = showStimulus2IFC(params,disparity,size,audio,...
                refDepth,refDepth,refDepth,part.interocularDistance,...
                allowResponse,squareColours,part.mapDir);
            % exit here is quit key was hit
            if params.isQuit(keyCode)
                break
            end
            % show the test, allowing a response
            allowResponse = 1;
            [keyCode,buttons,RT] = showStimulus2IFC(params,disparity,size,audio,...
                disparityDepth,otherDepth,otherDepth,part.interocularDistance,...
                allowResponse,squareColours,part.mapDir);
        else
            % show the test, without allowing a response
            allowResponse = 0;
            [keyCode,~,~] = showStimulus2IFC(params,disparity,size,audio,...
                disparityDepth,otherDepth,otherDepth,part.interocularDistance,...
                allowResponse,squareColours,part.mapDir);
            % exit here is quit key was hit
            if params.isQuit(keyCode)
                break
            end
            % show the test, allowing a response
            allowResponse = 1;
            [keyCode,buttons,RT] = showStimulus2IFC(params,disparity,size,audio,...
                refDepth,refDepth,refDepth,part.interocularDistance,...
                allowResponse,squareColours,part.mapDir);
        end
        
        % ----- GET A RESPONSE IF DIDN'T GET ONE WHILE SHOWING STIMULUS -----
        
        if ~params.isCloser(buttons) && ~params.isFurther(buttons) && ~params.isQuit(keyCode)
            [keyCode,buttons,RT] = getResponse(params,squareColours);
        end
        
        % ----- PROCESS RESPONSE -----
        
        % only do this if not quitting
        if ~params.isQuit(keyCode)
            
            % did they respond that second stimulus was closer?
            if params.isCloser(buttons)
                if refFirst % if reference was first they responded that test was closer
                    thisResponse = 'closer';
                else % if reference was second they responded that test was further
                    thisResponse = 'further';
                end
            elseif params.isFurther(buttons) % responded second was further
                if refFirst % if reference was first they responded that test was further
                    thisResponse = 'further';
                else % if reference was second they responded that test was closer
                    thisResponse = 'closer';
                end
            end
            
            % where to save this response
            if ~easyTrial
                % save to QUEST+ response list
                responses{length(QP.history_stim)+1,1} = thisResponse;
                % save reaction time
                reaction_times(length(QP.history_stim)+1,1) = RT;
                % update QUEST+
                QP.update(test, strcmp(responses{length(QP.history_stim)+1,1},'further'));
            else
                % save to easy trials response list
                easyCounter = easyCounter + 1;
                easy_depths(easyCounter,1) = test*q.halfRange + q.reference;
                easy_responses{easyCounter,1} = thisResponse;
                easy_reaction_times(easyCounter,1) = RT;
            end
            
        end

        % ----- SHOW FEEDBACK IF TRAINING TRIAL -----

        if strcmp(params.taskType,'training')

            % figure out if correct
            if (strcmp(thisResponse,'further') && test > 0) || ...
                    (strcmp(thisResponse,'closer') && test < 0)
                feedback = 'Correct';
            else
                feedback = 'Incorrect';
            end

            % show feedback
            showFeedback(params,feedback);

        end
        
        % check the if the quit key has been hit
        [~,~,keyCode] = KbCheck;
        
    end
    
    % ----- SAVE THE DATA FOR THIS CONDITION -----
    
    % save data as .csv
    reference = zeros([length(QP.history_stim),1]);
    test = QP.history_stim';
    response = responses(1:length(QP.history_stim),1);
    reaction_time = reaction_times(1:length(QP.history_stim),1);
    allTrials = table(reference,test,response,reaction_time);
    easyTrials = table(easy_depths,easy_responses,easy_reaction_times);
    fitParams = QP.getParamEsts('mean');
    PSE = fitParams(1);
    sensory_noise = sqrt((fitParams(2)^2)/2);
    lapse_rate = fitParams(4);
    JND = sqrt(2)*sensory_noise;
    measures = table(PSE,sensory_noise,lapse_rate,JND);
    writetable(allTrials,[params.parentFolder,'data\',part.pID,'\',part.pID,'_QUEST_',...
        params.taskType,'_',data.conditions{1,conditionCounter},'_trials.csv']);
    writetable(easyTrials,[params.parentFolder,'data\',part.pID,'\',part.pID,'_easy_',...
        params.taskType,'_',data.conditions{1,conditionCounter},'_trials.csv']);
    writetable(measures,[params.parentFolder,'data\',part.pID,'\',part.pID,'_',...
        params.taskType,'_',data.conditions{1,conditionCounter},'_measures.csv']);
    
    % output QUEST+ info
    data.QP = QP;
    data.responses = responses;
    data.RTs = reaction_times;
    
    % ----- DO SUBJECTIVE QUESTIONS -----
    
    if ismember(data.conditions{1,conditionCounter},{'S','D','A','SD','AD'}) && ...
            ~params.isQuit(keyCode)
        keyCode = runSubjective(params,part,...
            data.conditions{1,conditionCounter});
    end
    
end






