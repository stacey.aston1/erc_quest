function [keyCode,response] = getSubjectiveResponse(params,squareColours,...
    question,labels)

response = 0;
labelLocs = [0.25 0.5 0.75];
sliderPos = 0.5;
sliderStep = (labelLocs(1,end)-labelLocs(1,1))/100;

[~,~,keyCode] = KbCheck;
while ~response && ~params.isQuit(keyCode)
    
    for eye = {'left','right'}
        
        if strcmp(eye,'left')
            % Select left-eye image buffer for drawing (buffer = 0)
            Screen('SelectStereoDrawBuffer', params.window, 0);
        else
            % Select right-eye image buffer for drawing (buffer = 1)
            Screen('SelectStereoDrawBuffer', params.window, 1);
        end
        
        % Show question and instructions
        Screen('FillRect', params.window, params.backColour);
        firstOption = 'Press A to move left.';
        secondOption = 'Press B to move right.';
        continueText = 'Press Y to enter your response.';
        DrawFormattedText(params.window, question,...
            0.30*params.screenXpixels, 0.2*params.screenYpixels,...
            params.textColour);
        DrawFormattedText(params.window, firstOption ,...
            0.30*params.screenXpixels, 0.35*params.screenYpixels,...
            params.textColour);
        DrawFormattedText(params.window, secondOption,...
            0.30*params.screenXpixels, 0.4*params.screenYpixels,...
            params.textColour);
        DrawFormattedText(params.window, continueText,...
            0.30*params.screenXpixels, 0.45*params.screenYpixels,...
            params.textColour);
        
        % draw the frame
        Screen('DrawDots', params.window, [params.squareXcenters; params.squareYcenters], params.squareWidth, ...
            squareColours, [params.screenXpixels/2, params.screenYpixels/2], 4);
        
        % draw the response bar
        Screen('DrawLine', params.window, [0 0 0], ...
            labelLocs(1,1)*params.screenXpixels, 0.6*params.screenYpixels,...
            labelLocs(1,end)*params.screenXpixels, 0.6*params.screenYpixels,...
            7);
        
        % add some labels
        for l = 1:length(labels)
            DrawFormattedText2(labels{1,l}, 'win', params.window,...
            'sx', labelLocs(1,l)*params.screenXpixels, 'sy', 0.7*params.screenYpixels,...
            'xalign', 'center', 'baseColor',params.textColour);
        end
        
        % add the marker
        Screen('DrawDots', params.window, [sliderPos*params.screenXpixels, 0.6*params.screenYpixels], ...
            params.squareWidth, [1 1 1], [0, 0], 0);
    end
    
    % show it
    Screen('Flip', params.window);
    
    % wait for a response
    [~,~,keyCode] = KbCheck;
    [~,~,~,buttons] = WinJoystickMex(0);
    while ~params.isCloser(buttons) && ~params.isFurther(buttons) && ...
        ~params.isContinue(buttons) && ~params.isQuit(keyCode)
        [~,~,keyCode] = KbCheck;
        [~,~,~,buttons] = WinJoystickMex(0);
    end

    % process response
    if ~params.isQuit(keyCode)
        if params.isContinue(buttons)
            response =  (sliderPos-labelLocs(1,1))/(labelLocs(1,end)-labelLocs(1,1));
        elseif params.isCloser(buttons) % move left
            if sliderPos > labelLocs(1,1)
                sliderPos = sliderPos - sliderStep;
            end
        elseif params.isFurther(buttons) % move right
            if sliderPos < labelLocs(1,end)
                sliderPos = sliderPos + sliderStep;
            end
        end
    end
    
end

% check buttons are released
[~,~,keyCode] = KbCheck;
[~,~,~,buttons] = WinJoystickMex(0);
while (params.isCloser(buttons) || params.isFurther(buttons) || params.isContinue(buttons)) && ...
        ~params.isQuit(keyCode)
    [~,~,keyCode] = KbCheck;
    [~,~,~,buttons] = WinJoystickMex(0);
end

end







