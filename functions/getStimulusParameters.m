function params = getStimulusParameters(params)

% appearance parameters
params.textColour = [1 1 1];
params.dotColours = [0.1 0.1 0.1; 1 1 1];
params.backColour = [0.5 0.5 0.5];
params.fixationColour = [1 1 1];
params.squareColours = [0 0 0; 0.5 0.5 0.5; 1 1 1];

% distance from screen
params.distance = 138; % (distance for behavioural stuff in Dekker et al., 2015 was 50, 65 for scanner)

% screen height and width
params.height = 28.6; % in cm
params.width = 50.9; % in cm

% screen width in dov
params.widthD = 2*atand(params.width/(2*params.distance));

% how many pixels per degree?
params.pixelsPerDegree = params.screenXpixels/params.widthD;

% stimulus square in dov and converted to pixels
params.stimWidthD = 6; % (Dekker et al., 2015 had 11)
params.stimWidth = params.pixelsPerDegree*params.stimWidthD;
params.stimHeight = params.stimWidth;

% background rectangle in dov and converted to pixels
params.backWidthD = 16; % (Dekker et al., 2015 had 20)
params.backWidth = params.pixelsPerDegree*params.backWidthD;
params.backHeightD = 12; % (Dekker et al., 2015 had 16)
params.backHeight = params.pixelsPerDegree*params.backHeightD;

% gap between background and stimulus square (in dov and converted)
params.gapD = 1.5;
params.gap = params.pixelsPerDegree*params.gapD;

% required dot density (dots per square degree)
params.requiredDensity = 20; % (15 in Preston et al, 2008; ref 18 in Ban et al, 2012)

% how many dots do we need for required density?
params.nDots = round(params.backWidthD*params.backHeightD*params.requiredDensity);

% dot diameter (in dov and converted)
params.dotDiamD = 0.15;
params.dotDiam = params.pixelsPerDegree*params.dotDiamD;

% fixation cross line lengths (in dov and converted)
params.fixationD = 0.5;
params.fixation = params.pixelsPerDegree*params.fixationD;

% fixation cross hole width
params.holeD = 1;
params.hole = params.pixelsPerDegree*params.holeD;

% square width (in dov and converted)
params.squareWidthD = 0.3;
params.squareWidth = params.pixelsPerDegree*params.squareWidthD;

% border square centers
tempX = [-params.screenXpixels/2 + 4.5*params.squareWidth/2,...
    -params.screenXpixels/2 + 11.*params.squareWidth/2,...
    params.screenXpixels/2 - 11.*params.squareWidth/2,...
    params.screenXpixels/2 - 4.5.*params.squareWidth/2];
tempY = linspace(-params.screenYpixels/2 + params.squareWidth/2,...
    params.screenYpixels/2 - params.squareWidth/2,...
    15);
params.squareXcenters = [...
    repmat(tempX(1),[1,length(tempY)]), ... % outside left
    repmat(tempX(2),[1,length(tempY)]), ... % inside left
    repmat(tempX(end-1),[1,length(tempY)]), ... % inside right
    repmat(tempX(end),[1,length(tempY)])]; % outside right
params.squareYcenters = repmat(tempY',[length(tempX),1])'; % up right

% mean audio frequency (mapped to background, or depth of screen)
params.meanFreq = 600;

% Samples per second. Should at least double the max frequency.
% 44.1kHz is also okay.
params.Fs = 48000;

% stimulus presentation times (in seconds)
params.stimTime = 1;
params.isi = 0.5;

% response time limit (in seconds)
params.timeLimit = 1000; % no real time limit in these tasks

% whether or not to check for a response during stimulus presentation, we
% might not want to if only showing for set time (e.g., 2IFC task), or we
% might leave the stimulus on screen until response (or timed out)
params.checkResponse = 1;

% add some buttons we need for the tasks (on the XBox Controller)
params.buttonIDs{1,1} = 'closer';
params.buttonInds(1,1) = 1; 
params.buttonIDs{2,1} = 'further';
params.buttonInds(2,1) = 2;
params.buttonIDs{3,1} = 'neither';
params.buttonInds(3,1) = 3;
params.buttonIDs{4,1} = 'continue';
params.buttonInds(4,1) = 4;

% set up button checks
params.isCloser = @(buttons) buttons(params.buttonInds(1,1));
params.isFurther = @(buttons) buttons(params.buttonInds(2,1));
params.isNeither = @(buttons) buttons(params.buttonInds(3,1));
params.isContinue = @(buttons) buttons(params.buttonInds(4,1));

% how long to show feedback for (in s)
params.feedbackDuration = 0.5;

end













