function [data,params,q] = setUpStimuli(params,part)

% attached participant info to data structure
data.part = part;

% conditions
if strcmp(params.taskType,'training')
    conditions = {'S','D','SD','AD'};
    conditions = conditions(randperm(length(conditions)));
    % save this order to be used again later
    save([params.parentFolder,'data\',part.pID,'_',...
        'training_order.mat'],'conditions')
    % add to data structure
    data.conditions = conditions;
elseif ismember(params.taskType,{'combination','noisy_combination'})
    if strcmp(params.taskType,'combination')
        % load the training order and add audio
        try
            load([params.parentFolder,'data\',part.pID,'_',...
                'training_order.mat'],'conditions')
        catch
            error('No training data for this participant number.')
        end
        % insert audio cue
        pos = find(strcmp(conditions,'AD'));
        if pos == 4
            conditions = [conditions,'A'];
        else
            conditions = [conditions(1,1:pos),'A',conditions(1,pos+1:end)];
        end
        % save this order to be used again later
        save([params.parentFolder,'data\',part.pID,'_',...
            'combination_order.mat'],'conditions')
        % add to data structure
        data.conditions = conditions;
    else % load the previous reweighting order
        try
            load([params.parentFolder,'data\',part.pID,'_',...
                'combination_order.mat'],'conditions')
        catch
            error('No noiseless combination data for this participant number.')
        end
        % add to data structure
        data.conditions = conditions;
    end
elseif ismember(params.taskType,'fusion')
    % load the combination order and add incongruent conditions
    try
        load([params.parentFolder,'data\',part.pID,'_',...
            'combination_order.mat'],'conditions')
    catch
        error('No noiseless combination data for this participant number.')
    end
    % randomly order fusion blocks
    fusion_conditions = {'SDi','ADi'};
    fusion_conditions = fusion_conditions(randperm(length(fusion_conditions)));
    data.conditions = [conditions,fusion_conditions];
end

% what cues are present in each condition?
data.disparityPresent = nan(1,length(data.conditions));
data.sizePresent = nan(1,length(data.conditions));
data.audioPresent = nan(1,length(data.conditions));
for c = 1:length(data.conditions)
    if strcmp(data.conditions{1,c},'S')
        data.disparityPresent(1,c) = 0;
        data.sizePresent(1,c) = 1;
        data.audioPresent(1,c) = 0;
    elseif strcmp(data.conditions{1,c},'D')
        data.disparityPresent(1,c) = 1;
        data.sizePresent(1,c) = 0;
        data.audioPresent(1,c) = 0;
    elseif strcmp(data.conditions{1,c},'A')
        data.disparityPresent(1,c) = 0;
        data.sizePresent(1,c) = 0;
        data.audioPresent(1,c) = 1;
    elseif ismember(data.conditions{1,c},{'SD','SDi'})
        data.disparityPresent(1,c) = 1;
        data.sizePresent(1,c) = 1;
        data.audioPresent(1,c) = 0;
    elseif ismember(data.conditions{1,c},{'AD','ADi'})
        data.disparityPresent(1,c) = 1;
        data.sizePresent(1,c) = 0;
        data.audioPresent(1,c) = 1;
    end
end

% get (normalised) QUEST+ parameters
q = getQuestPlusParams(params.maxTrials);

% non-normalised parameters
if strcmp(part.group,'F')
    q.reference = 133;
    q.halfRange = 4; % 129 to 137
elseif strcmp(part.group,'B')
    q.reference = 143;
    q.halfRange = 4; % 139 to 147
end

% define the reference waveform
params.reference = q.reference;
semitoneDiff = getSemitoneDifference(params.distance,params.reference,part.mapDir);
params.refWaveform = getWaveform(semitoneDiff,params);

end





