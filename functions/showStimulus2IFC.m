function [keyCode,buttons,RT] = showStimulus2IFC(params,disparity,size,audio,...
    disparityDepth,sizeDepth,audioDepth,interocular,allowResponse,...
    squareColours,mapDir)

% get the left/right eye offset if showing disparity
if disparity
    % get the noisy dot depths
    disparityDepths = randn(1,params.nDots) .* params.sdJit;
    % make sure dots average to true depth
    disparityDepths = disparityDepths - mean(disparityDepths) + disparityDepth;
    % get the offset for each dot
    offsets = getDisparity(params.distance,disparityDepths,interocular);
    % convert offsets to pixels
    rightShiftPix = (offsets ./ params.width) .* params.screenXpixels;
else
    rightShiftPix = zeros(1,params.nDots);
end

% get the change in the stimulus sizes if showing size
if size
    % stimulus square
    stimulusWidthD = getSize(params.distance,sizeDepth,params.stimWidthD);
    % convert to pixels
    stimulusWidth = params.pixelsPerDegree*stimulusWidthD;
    % stimulus dots
    dotDiameterD = getSize(params.distance,sizeDepth,params.dotDiamD);
    % convert to pixels
    dotDiameter = params.pixelsPerDegree*dotDiameterD;
else
    stimulusWidth = params.stimWidth;
    dotDiameter = params.dotDiam;
end

% get the waveform if playing sound
if audio
    if audioDepth == params.reference
        PsychPortAudio('FillBuffer', params.audioHandle, params.refWaveform);
    else
        semitoneDiff = getSemitoneDifference(params.distance,audioDepth,mapDir);
        waveform = getWaveform(semitoneDiff,params);
        PsychPortAudio('FillBuffer', params.audioHandle, waveform);
    end
end

% get random dot positions
dotPosX = rand(1,params.nDots)*params.backWidth - params.backWidth/2;
dotPosY = rand(1,params.nDots)*params.backHeight - params.backHeight/2;

% pull out background dots
backDotInds = find(-(params.stimWidth/2 + params.gap) > dotPosX | ...
    dotPosX > params.stimWidth/2  + params.gap | ...
    -(params.stimWidth/2  + params.gap) > dotPosY | ...
    dotPosY > params.stimWidth/2  + params.gap);
dotPosXback = dotPosX(backDotInds);
dotPosYback = dotPosY(backDotInds);

% find all stimulus dots (removing central square for fixation) and shift
stimDotInds = find((-stimulusWidth/2 <= dotPosX & ...
    dotPosX <= stimulusWidth/2 & ...
    -stimulusWidth/2 <= dotPosY & ...
    dotPosY <= stimulusWidth/2) & ...
    ~(-(params.hole/2 + dotDiameter) <= dotPosX & ...
    dotPosX <= params.hole/2 + dotDiameter & ...
    -(params.hole/2 + dotDiameter) <= dotPosY & ...
    dotPosY <= params.hole/2 + dotDiameter));
dotPosXleft = dotPosX(stimDotInds) - rightShiftPix(stimDotInds);
dotPosXright = dotPosX(stimDotInds) + rightShiftPix(stimDotInds);
dotPosYstim = dotPosY(stimDotInds);

% set the random dot colours
stimulusColours = params.dotColours(randi(2,[length(dotPosXright),1]),:)';
backColours = params.dotColours(randi(2,[length(dotPosXback),1]),:)';

% ----- BACKGROUND -----

for eye = {'left','right'}
    
    if strcmp(eye,'left')
        % Select left-eye image buffer for drawing (buffer = 0)
        Screen('SelectStereoDrawBuffer', params.window, 0);
    else
        % Select right-eye image buffer for drawing (buffer = 1)
        Screen('SelectStereoDrawBuffer', params.window, 1);
    end
    
    % show the background
    Screen('FillRect', params.window, params.backColour);
    
    % draw fixation cross
    Screen('DrawLine',params.window,params.fixationColour, ...
        params.xCenter-params.fixation/2, params.yCenter, ...
        params.xCenter+params.fixation/2, params.yCenter, ...
        params.dotDiam/2)
    Screen('DrawLine',params.window,params.fixationColour, ...
        params.xCenter, params.yCenter-params.fixation/2, ...
        params.xCenter, params.yCenter+params.fixation/2, ...
        params.dotDiam/2)
    
    % draw the frame
    Screen('DrawDots', params.window, [params.squareXcenters; params.squareYcenters], params.squareWidth, ...
        squareColours, [params.screenXpixels/2, params.screenYpixels/2], 4);
    
end

% show it
Screen('Flip', params.window);

% wait for the length of the inter-stimulus interval
tic;
[~,~,keyCode] = KbCheck; 
while ~params.isQuit(keyCode) && toc < params.isi
   [~,~,keyCode] = KbCheck; 
end

% ----- ADD STIMULUS -----

% start the sound
if audio
    PsychPortAudio('Start', params.audioHandle, 1, 0);
end

% Select left-eye image buffer for drawing (buffer = 0)
Screen('SelectStereoDrawBuffer', params.window, 0);

% draw common stimuli for left eye
drawCommonStimuli(params,dotPosXback,dotPosYback,backColours,squareColours);

if disparity || size
    % draw left dots
    Screen('DrawDots', params.window, [dotPosXleft; dotPosYstim], dotDiameter, ...
        stimulusColours, [params.screenXpixels/2, params.screenYpixels/2], 2);
end

% Select right-eye image buffer for drawing (buffer = 1)
Screen('SelectStereoDrawBuffer', params.window, 1);

% draw common stimuli for right eye
drawCommonStimuli(params,dotPosXback,dotPosYback,backColours,squareColours);

if disparity || size
    % draw right dots
    Screen('DrawDots', params.window, [dotPosXright; dotPosYstim], dotDiameter, ....
        stimulusColours, [params.screenXpixels/2, params.screenYpixels/2], 2);
end

% show it
Screen('Flip', params.window);

% wait for the length of the inter-stimulus interval or until a button is
% pressed if allowing responses
tic;
[~,~,~,buttons] = WinJoystickMex(0);
while (~allowResponse && (~params.isQuit(keyCode) && toc < params.stimTime)) || ...
        (allowResponse && (~params.isQuit(keyCode) && ~params.isCloser(buttons) && ...
        ~params.isFurther(buttons) && toc < params.stimTime))
   [~,~,keyCode] = KbCheck;
   [~,~,~,buttons] = WinJoystickMex(0);
end
RT = toc;

% stop the audio
if audio
    PsychPortAudio('Stop', params.audioHandle);
end

end

function drawCommonStimuli(params,dotPosXback,dotPosYback,backColours,...
    squareColours)

% show the background
Screen('FillRect', params.window, params.backColour);

% draw fixation cross
Screen('DrawLine',params.window,params.fixationColour, ...
    params.xCenter-params.fixation/2, params.yCenter, ...
    params.xCenter+params.fixation/2, params.yCenter, ...
    params.dotDiam/2)
Screen('DrawLine',params.window,params.fixationColour, ...
    params.xCenter, params.yCenter-params.fixation/2, ...
    params.xCenter, params.yCenter+params.fixation/2, ...
    params.dotDiam/2)

% draw background dots
Screen('DrawDots', params.window, [dotPosXback;dotPosYback], params.dotDiam,...
    backColours, [params.screenXpixels/2, params.screenYpixels/2], 2);

% draw the frame
Screen('DrawDots', params.window, [params.squareXcenters; params.squareYcenters], params.squareWidth, ...
    squareColours, [params.screenXpixels/2, params.screenYpixels/2], 4);

end
















