function part = runAssesment(params,part)

% ----- SET UP TRIALS -----
data = setUpAssesmentStimuli();

% ----- EXPERIMENT LOOP -----

% check if the quit key has been hit
[~,~,keyCode] = KbCheck;

% run through each trial unless the quit key is hit
trialNumber = 0;
while ~params.isQuit(keyCode) && trialNumber < length(data.depths)
    
    % next trial
    trialNumber = trialNumber + 1;
    
    % ----- SHOW THE STIMULI -----
    
    % set the square colours for this trial
    squareColours = params.squareColours(randi(3,[length(params.squareXcenters),1]),:)';
    
    % get this test depth
    testDepth = data.depths(trialNumber,1);
    
    % show the test, without allowing a response
    allowResponse = 0;
    [keyCode,~,~] = showStimulus2IFC(params,data.disparityPresent,...
        data.sizePresent,data.audioPresent,...
        testDepth,testDepth,testDepth,part.interocularDistance,...
        allowResponse,squareColours);
    % exit here is quit key was hit
    if params.isQuit(keyCode)
        break
    end
    
    % ----- GET A RESPONSE -----
    
    if ~params.isQuit(keyCode)
        [keyCode,buttons,~] = getAssesmentResponse(params,squareColours);
    end
    
    % ----- PROCESS RESPONSE -----
    
    % only do this if not quitting
    if ~params.isQuit(keyCode)
        % did they respond that object was in front?
        if params.isCloser(buttons)
            data.responses{trialNumber,1} = 'infront';
        elseif params.isFurther(buttons) % responded it was behind
            data.responses{trialNumber,1} = 'behind';
        elseif params.isNeither(buttons) % responded it was neither
            data.responses{trialNumber,1} = 'neither';
        end
    end
    
    % check the if the quit key has been hit
    [~,~,keyCode] = KbCheck;
    
end

% save the data in the part structure and decide what side to assign to
part.assesmentDepths = data.depths;
part.assesmentResponses = data.responses;
part.assesmentPercentages = nan(1,3);
uniqueDepths = unique(data.depths);
for d = 1:length(uniqueDepths)
    inds = find(data.depths == uniqueDepths(d));
    if uniqueDepths(d) < 138
        nCorrect = sum(strcmp(data.responses(inds,1),'infront'));
        part.assesmentPercentages(1,d) = nCorrect*10;
    elseif uniqueDepths(d) > 138
        nCorrect = sum(strcmp(data.responses(inds,1),'behind'));
        part.assesmentPercentages(1,d) = nCorrect*10;
    else
        nCorrect = sum(strcmp(data.responses(inds,1),'neither'));
        part.assesmentPercentages(1,d) = nCorrect*20;
    end
end

% assign participant to a group
load([params.parentFolder,'data\group_counts.mat'],'groupCounts')
% assign to front group if they can do it and if any more are needed
nPerGroup = 3;
successRate = 80;
if groupCounts.front < nPerGroup && part.assesmentPercentages(1,1) >= successRate
    part.group = 'F';
    groupCounts.front = groupCounts.front + 1; 
% or assign to back group if they can do it and more are needed
elseif groupCounts.back < nPerGroup && part.assesmentPercentages(1,3) >= successRate
    part.group = 'B';
    groupCounts.back = groupCounts.back + 1;
% otherwise, this person cannot do the experiment
else
    part.group = 'None';
end
if ~part.demo
    save([params.parentFolder,'data\group_counts.mat'],'groupCounts')
end
group = part.group;
interocular = part.interocularDistance;
mapDir = part.mapDir;
save([params.parentFolder,'data\',part.pID,'\',part.pID,'_','group.mat'],...
    'group','interocular','mapDir')

% say thanks if they are excluded base don initial assesment
if strcmp(part.group,'None')
    text = ['Unfortunately, we were unable to assign you to a participant \n\n',...
        'group based on the screening test. This means we cannot continue the \n\n',...
        'study any further. We are sorry for any inconvenience and grateful for \n\n',...
        'your time. Please speak with the experimenter. \n\n\n',...
        'Press Y to continue.'];
    textBlock(params,text);
end

end









