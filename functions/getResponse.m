function [keyCode,buttons,RT] = getResponse(params,squareColours)

for eye = {'left','right'}
    
    if strcmp(eye,'left')
        % Select left-eye image buffer for drawing (buffer = 0)
        Screen('SelectStereoDrawBuffer', params.window, 0);
    else
        % Select right-eye image buffer for drawing (buffer = 1)
        Screen('SelectStereoDrawBuffer', params.window, 1);
    end
    
    % Show question and instructions
    Screen('FillRect', params.window, params.backColour);
    question = ['Was the second object closer to you or \n\n' ...
        'further away than the first?'];
    firstOption = 'Press A if it was closer to you.';
    secondOption = 'Press B if it was further away.';
    DrawFormattedText(params.window, question,...
        0.30*params.screenXpixels, 0.25*params.screenYpixels,...
        params.textColour);
    DrawFormattedText(params.window, firstOption ,...
        0.30*params.screenXpixels, 0.50*params.screenYpixels,...
        params.textColour);
    DrawFormattedText(params.window, secondOption,...
        0.30*params.screenXpixels, 0.60*params.screenYpixels,...
        params.textColour);
    
    % draw the frame
    Screen('DrawDots', params.window, [params.squareXcenters; params.squareYcenters], params.squareWidth, ...
        squareColours, [params.screenXpixels/2, params.screenYpixels/2], 4);
    
end

% show it
Screen('Flip', params.window);

% wait for a response
[~,~,keyCode] = KbCheck;
[~,~,~,buttons] = WinJoystickMex(0);
while ~params.isCloser(buttons) && ~params.isFurther(buttons) && ...
        ~params.isQuit(keyCode) && toc < params.timeLimit
    [~,~,keyCode] = KbCheck;
    [~,~,~,buttons] = WinJoystickMex(0);
end
RT = toc;

end