function [data,params] = runInterleavedStaircases(params,part)

% ----- SET UP TRIALS -----
[data,params,q] = setUpInterleavedStimuli(params,part);

% ----- EXPERIMENT LOOP -----

% initialise the condition counter
conditionCounter = 0;

% check if the quit key has been hit
[~,~,keyCode] = KbCheck;

% initialise easy trial tracker
easyTrial = 0;
easyCounter = 0;
easy_depths = nan(floor(q.stopCriterion/10)-1,1);
easy_responses = cell(floor(q.stopCriterion/10)-1,1);
easy_reaction_times = nan(floor(q.stopCriterion/10)-1,1);

% break message
breakMessage1 = 'Well done! You have finished block ';
breakMessage2 = ' more to go!';
breakMessage3 = 'Please take a break and press Y when you are ready to continue';

% run through each block (condition) unless the quit key is hit
while ~params.isQuit(keyCode) && conditionCounter < length(data.conditions)
    
    % ----- TIME TO TAKE A BREAK -----

    if conditionCounter
        totalConds = length(data.conditions) + 5;
        currentCond = conditionCounter;
        leftConds = totalConds - currentCond - 5;
        breakText = [breakMessage1,num2str(currentCond),' \n\n ',...
            num2str(leftConds),breakMessage2,' \n\n',...
            breakMessage3];
        textBlock(params,breakText);
    end

    % ----- COUNT UP THE CONDITION COUNTER AND SET UP THIS STAIRCASE -----
    
    % next condition
    conditionCounter = conditionCounter + 1;
    
    % set up a new QUEST+ objects
    QPneg = QuestPlus(q.F, q.stimDomain, q.paramDomain, q.respDomain, ...
        q.stopRule, q.stopCriterion);
    QPpos = QuestPlus(q.F, q.stimDomain, q.paramDomain, q.respDomain, ...
        q.stopRule, q.stopCriterion);
    
    % initialise with uniform priors (possibly from saved file to save
    % time)
    q.priors = {ones(1,length(q.paramDomain{1}))/length(q.paramDomain{1}), ...
        ones(1,length(q.paramDomain{2}))/length(q.paramDomain{2}), ...
        ones(1,length(q.paramDomain{3}))/length(q.paramDomain{3}), ...
        ones(1,length(q.paramDomain{4}))/length(q.paramDomain{4})};
    q.fn = [params.parentFolder,'functions\QuestPlusLikelihoodsExp1.mat'];
    if exist(q.fn, 'file')
        QPneg.initialise(q.priors, q.fn);
        QPpos.initialise(q.priors, q.fn);
    else
        QPneg.initialise(q.priors);
        QPneg.saveLikelihoods(q.fn);
        QPpos.initialise(q.priors);
        QPpos.saveLikelihoods(q.fn);
    end

    % ----- GET THE VALUE OF DELTA -----
    if strcmp(data.conditions{1,conditionCounter},'SD+-')
        Tab = readtable([params.parentFolder,'data\',part.pID,'\',part.pID,'_',...
            params.taskType,'_S_measures.csv']);
    else
        Tab = readtable([params.parentFolder,'data\',part.pID,'\',part.pID,'_',...
            params.taskType,'_A_measures.csv']);
    end
    absDelta = 1.5*Tab{1,'JND'};

    % ----- PROCESS CUE INFORMATION -----
    
    % find out which cues to display in this block
    
    disparity = data.disparityPresent(1,conditionCounter);
    size = data.sizePresent(1,conditionCounter);
    audio = data.audioPresent(1,conditionCounter);
    
    % ----- RUN THIS BLOCK OF TRIALS -----
    
    % where to save responses
    responses = cell(QPneg.maxNTrials,2);
    
    % where to save RTs
    reaction_times = nan(QPneg.maxNTrials,2);
    
    % estimate parameters using QUEST+
    while (~QPneg.isFinished() || ~QPpos.isFinished()) && ~params.isQuit(keyCode)
        
        % check if we are due an easy trial
        if (~isempty(QPneg.history_stim) || ~isempty(QPpos.history_stim)) && ...
                ~mod(length(QPneg.history_stim)+length(QPpos.history_stim),params.easyInt) && ...
                ~params.isQuit(keyCode) && ~easyTrial
            % time for a memory dump (just incase) and an easy trial
            save([params.parentFolder,'memoryDumps\temp',params.compID{1,1},'\',part.pID,'_',...
                params.taskType,'_',data.conditions{1,conditionCounter},...
                '_',datestr(now,'dd-mmm-yyyy-HH-MM-SS')])
            easyTrial = 1;
        elseif (~isempty(QPneg.history_stim) || ~isempty(QPpos.history_stim)) && ...
                ~mod(length(QPneg.history_stim)+length(QPpos.history_stim),params.progressInt) && ...
                ~params.isQuit(keyCode)
                totalConds = length(data.conditions) + 5;
            currentCond = conditionCounter + 5;
            percent = round((length(QPneg.history_stim)+length(QPpos.history_stim))/...
                (QPpos.maxNTrials*2)*100);
            progText = ['Well done! You have completed ',num2str(percent),'% of trials in \n\n',...
                ' condition ',num2str(currentCond),' of ',num2str(totalConds),' \n\n\n',...
                'Press Y to continue'];
            textBlock(params,progText);
            easyTrial = 0;
        else
            easyTrial = 0;
        end

        % are we doing an easy trial or not?
        if ~easyTrial
            % get the reference depth
            refDepth = q.reference;
            % pick a staircase to do next
            goodStaircase = 0;
            while ~goodStaircase
                % choose positive at random if not finished
                if rand > 0.5 && ~QPpos.isFinished()
                    doPos = 1; goodStaircase = 1;
                elseif ~QPneg.isFinished() % or do negative if not finished
                    doPos = 0; goodStaircase = 1;
                end
            end
            % get next test stimulus
            if doPos
                test = QPpos.getTargetStim();
                delta = absDelta;
            else
                test = QPneg.getTargetStim();
                delta = -absDelta;
            end
        else
            % get the reference depth
            refDepth = -0.5*q.halfRange + q.reference;
            % get the test depth
            test = 0.5;
            delta = 0;
        end
        % convert to a depth to show
        testDepth = test*q.halfRange + q.reference;
        otherDepth = delta*q.halfRange + q.reference;
        
        % ----- SHOW THE STIMULI -----
        
        % set the square colours for this trial
        squareColours = params.squareColours(randi(3,[length(params.squareXcenters),1]),:)';
        
        % randomly decide to show reference first or second
        refFirst = rand > 0.5;
        if refFirst
            % show the reference, without allowing a response
            allowResponse = 0;
            [keyCode,~,~] = showStimulus2IFC(params,disparity,size,audio,...
                refDepth,otherDepth,otherDepth,part.interocularDistance,...
                allowResponse,squareColours,part.mapDir);
            % exit here is quit key was hit
            if params.isQuit(keyCode)
                break
            end
            % show the test, allowing a response
            allowResponse = 1;
            [keyCode,buttons,RT] = showStimulus2IFC(params,disparity,size,audio,...
                testDepth,testDepth,testDepth,part.interocularDistance,...
                allowResponse,squareColours,part.mapDir);
        else
            % show the test, without allowing a response
            allowResponse = 0;
            [keyCode,~,~] = showStimulus2IFC(params,disparity,size,audio,...
                testDepth,testDepth,testDepth,part.interocularDistance,...
                allowResponse,squareColours,part.mapDir);
            % exit here is quit key was hit
            if params.isQuit(keyCode)
                break
            end
            % show the test, allowing a response
            allowResponse = 1;
            [keyCode,buttons,RT] = showStimulus2IFC(params,disparity,size,audio,...
                refDepth,otherDepth,otherDepth,part.interocularDistance,...
                allowResponse,squareColours,part.mapDir);
        end
        
        % ----- GET A RESPONSE IF DIDN'T GET ONE WHILE SHOWING STIMULUS -----
        
        if ~params.isCloser(buttons) && ~params.isFurther(buttons) && ~params.isQuit(keyCode)
            [keyCode,buttons,RT] = getResponse(params,squareColours);
        end
        
        % ----- PROCESS RESPONSE -----
        
        % only do this if not quitting
        if ~params.isQuit(keyCode)
            
            % did they respond that second stimulus was closer?
            if params.isCloser(buttons)
                if refFirst % if reference was first they responded that test was closer
                    thisResponse = 'closer';
                else % if reference was second they responded that test was further
                    thisResponse = 'further';
                end
            elseif params.isFurther(buttons) % responded second was further
                if refFirst % if reference was first they responded that test was further
                    thisResponse = 'further';
                else % if reference was second they responded that test was closer
                    thisResponse = 'closer';
                end
            end
            
            % where to save this response
            if ~easyTrial
                if doPos
                    % save to QUEST+ response list
                    responses{length(QPpos.history_stim)+1,2} = thisResponse;
                    % save reaction time
                    reaction_times(length(QPpos.history_stim)+1,2) = RT;
                    % update QUEST+
                    QPpos.update(test, strcmp(responses{length(QPpos.history_stim)+1,2},'further'));
                else
                    % save to QUEST+ response list
                    responses{length(QPneg.history_stim)+1,1} = thisResponse;
                    % save reaction time
                    reaction_times(length(QPneg.history_stim)+1,1) = RT;
                    % update QUEST+
                    QPneg.update(test, strcmp(responses{length(QPneg.history_stim)+1,1},'further'));
                end
            else
                % save to easy trials response list
                easyCounter = easyCounter + 1;
                easy_depths(easyCounter,1) = testDepth;
                easy_responses{easyCounter,1} = thisResponse;
                easy_reaction_times(easyCounter,1) = RT;
            end
            
        end
        
        % check the if the quit key has been hit
        [~,~,keyCode] = KbCheck;
        
    end
    
    % ----- SAVE THE DATA FOR THIS CONDITION -----
    
    % positive data
    reference= zeros([length(QPpos.history_stim),1]);
    test = QPpos.history_stim';
    response = responses(1:length(QPpos.history_stim),2);
    reaction_time = reaction_times(1:length(QPpos.history_stim),2);
    allTrialsPos = table(reference,test,response,reaction_time);

    % negative data
    reference= zeros([length(QPneg.history_stim),1]);
    test = QPneg.history_stim';
    response = responses(1:length(QPneg.history_stim),1);
    reaction_time = reaction_times(1:length(QPneg.history_stim),1);
    allTrialsNeg = table(reference,test,response,reaction_time);

    % easy trials
    easyTrials = table(easy_depths,easy_responses,easy_reaction_times);

    % parameter estimates
    fitParamsPos = QPpos.getParamEsts('mean');
    fitParamsNeg = QPneg.getParamEsts('mean');
    PSE = [fitParamsPos(1);fitParamsNeg(1)];
    sensory_noise = [sqrt((fitParamsPos(2)^2)/2);sqrt((fitParamsNeg(2)^2)/2)];
    lapse_rate = [fitParamsPos(4);fitParamsNeg(4)];
    JND = sqrt(2).*sensory_noise;
    measures = table(PSE,sensory_noise,lapse_rate,JND);

    % save all as csv
    writetable(allTrialsPos,[params.parentFolder,'data\',part.pID,'\',part.pID,'_QUEST_',...
        params.taskType,'_',data.conditions{1,conditionCounter},'_pos_trials.csv']);
    writetable(allTrialsNeg,[params.parentFolder,'data\',part.pID,'\',part.pID,'_QUEST_',...
        params.taskType,'_',data.conditions{1,conditionCounter},'_neg_trials.csv']);
    writetable(easyTrials,[params.parentFolder,'data\',part.pID,'\',part.pID,'_easy_',...
        params.taskType,'_',data.conditions{1,conditionCounter},'_trials.csv']);
    writetable(measures,[params.parentFolder,'data\',part.pID,'\',part.pID,'_',...
        params.taskType,'_',data.conditions{1,conditionCounter},'_measures.csv']);
    
    % output QUEST+ info
    data.QPpos = QPpos;
    data.QPneg = QPneg;
    data.responses = responses;
    data.RTs = reaction_times;

end






