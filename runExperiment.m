% Clear the workspace
clc; clear; close all;
sca;

% close the audio port if already open
try
    PsychPortAudio('Close');
catch
    disp('Tried to close PsychPortAudio but not open')
end

%% ----- DEPENDENCIES -----

% path to parent folder
params.parentFolder = [pwd,'\'];

% add path to QuestPlus code
addpath(genpath([params.parentFolder,'dependencies\QuestPlus']))

% add path to the functions we need
addpath(genpath([params.parentFolder,'functions']))

%% ----- SET UP PSYCHTOOLBOX -----

% set a steromode
params.stereoMode = 8; % for red-blue, but we will fine tune later

% Some default settings
PsychDefaultSetup(2);
Screen('Preference', 'SkipSyncTests', 1);

% Setup Psychtoolbox for OpenGL 3D rendering support and initialize the
% mogl OpenGL for Matlab wrapper
InitializeMatlabOpenGL;

% Random number generator
rng('shuffle');

% Get the screen numbers. This gives us a number for each of the screens
% attached to our computer.
params.screens = Screen('Screens');
  
% Defines the screen to use
params.screenNumber = max(params.screens);
 
% Open a window
[params.window, params.windowRect] = PsychImaging('OpenWindow', params.screenNumber, ...
    0, [], 32, 2, params.stereoMode);

% Get the size of the on screen window in pixels
[params.screenXpixels, params.screenYpixels] = Screen('WindowSize', params.window);

% Get the centre coordinate of the window in pixels
[params.xCenter, params.yCenter] = RectCenter(params.windowRect);

% fine tune the colour gains
SetAnaglyphStereoParameters('LeftGains', params.window, [0.4 0.0 0.0]);
SetAnaglyphStereoParameters('RightGains', params.window, [0.0 0.2 0.7]);

% Set up alpha-blending for smooth (anti-aliased) edges to our dots
Screen('BlendFunction', params.window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');

% set up a quit button
KbName('UnifyKeyNames');
params.keyIDs{1,1} = 'quit';
params.keyInd(1,1) = KbName('ESCAPE'); 

% set up keyCode checks
params.isQuit = @(keyCode) keyCode(params.keyInd(1,1));
 
% Hide the cursor   
HideCursor; 

% text settings
Screen('TextFont', params.window, 'Ariel');
Screen('TextSize', params.window, 30);

% make a temp folder for the occasional memory dump in case of crashes
[~,params.compID] = system('hostname');
params.compID = cellstr(params.compID(1:end-1));
mkdir([params.parentFolder,'memoryDumps\'],['temp',params.compID{1,1}]) 

%% ----- GET GENERAL STIMULUS PARAMETERS -----

params = getStimulusParameters(params);

%% ----- AUDIO SET UP -----

% audio set up
InitializePsychSound(1);
params.audioHandle = PsychPortAudio('Open', [], [], 2, params.Fs, 1);

%% ----- GET INFORMATION ABOUT THE PARTICIPANT -----

Screen('FillRect', params.window, params.backColour);
part.pID = GetEchoString(params.window, 'Enter Participant ID (ERC_Exp1_X):',...
    params.screenXpixels*0.1,params.screenYpixels*0.35,params.textColour,params.backColour,0,2,[],[]);
part.session = str2double(GetEchoString(params.window, 'Session Number (1-4):',...
    params.screenXpixels*0.1,params.screenYpixels*0.45,params.textColour,params.backColour,0,2,[],[]));
part.demo = str2double(GetEchoString(params.window, 'Run demo (1 = yes, 0 = no):',...
    params.screenXpixels*0.1,params.screenYpixels*0.55,params.textColour,params.backColour,0,2,[],[]));
part.date = datestr(now);

% check the information that has been entered
if ~any(part.session == 1:4)
    sca; error('Session must be 1, 2, 3, or 4');
end

%% ----- GET EXTRA INFORMATION IF THIS IS THE FIRST DAY -----

if part.session == 1
    Screen('FillRect', params.window, params.backColour);
    % get age
    part.age = str2double(GetEchoString(params.window, 'Enter Participant Age (in years):',...
        params.screenXpixels*0.1,params.screenYpixels*0.35,params.textColour,params.backColour,0,2,[],[]));
    % check the information that has been entered
%     if ~any(part.age == 18:35)
%         sca; error('Age must be in the range 18 to 35');
%     end
    % get sex
    part.sex = 'Female';GetEchoString(params.window, 'Enter Participant Sex At Birth (Female, Male, Don''t Know, or Prefer Not To Say):',...
        params.screenXpixels*0.1,params.screenYpixels*0.45,params.textColour,params.backColour,0,2,[],[]);
    % check the information that has been entered
    if ~ismember(part.sex,{'Female','Male','Don''t Know','Prefer Not To Say'})
        sca; error('Sex must be one of Female, Male, Don''t Know, or Prefer Not To Say');
    end
    % get interocular distance
    part.interocularDistance = str2double(GetEchoString(params.window, 'Enter Interocular Distance (in cm):',...
        params.screenXpixels*0.1,params.screenYpixels*0.55,params.textColour,params.backColour,0,2,[],[]));
    % check the information that has been entered
    if part.interocularDistance < 1 || part.interocularDistance > 10
        sca; error('Interocular distance must be a number between 1 and 10');
    end
    % randomly decided an audio direction for this P
    part.mapDir = (-1)^(rand>0.5);
    % make a directory for this P
    mkdir([params.parentFolder,'data\',part.pID]) 
end

%% ----- INSTRUCTIONS -----

% pre assesment instructions
preText = ['Welcome to the pre-assessment task! \n\n\n',...
    'In this task you will see an object (a square) presented on screen. The object \n\n',...
    'will appear at different distances, that is, it will either look \n\n',...
    'like it is in front of the screen (closer to you), like it is behind \n\n',...
    'the screen (further from you), or neither. In this task, we will show you \n\n',...
    'one object at a time, and you will need to judge whether it is in front \n\n',...
    'of the screen, behind the screen, or neither. \n\n\n',...
    'Please press the controller key A to indicate that it is in front (closer) \n\n',...
    'Press the controller key B to indicate that it is behind (further) \n\n',...
    'Press the controller key X to indicate that it is neither in front or behind \n\n\n',...
    'Please press Y to start the task'];

% training instructions
trainText = ['Welcome to the experiment! \n\n\n',...
    'In this task you will be presented with two objects one after the other. \n\n',...
    'The objects will always be on one side of the screen, that is it is always \n\n',...
    'in front or behind. However, on each of the two presentations, the objects will be \n\n',...
    'located at different distances. Compared to the first object, the second \n\n',...
    'object will either be slightly closer to you, or it will be slightly further \n\n',...
    'away from you. We want you to judge whether the second object was closer or \n\n',...
    'further away than the first object. \n\n\n',...
    'Please press key A to indicate that the second object was closer than the first \n\n',...
    'Please press key B to indicate that the second object was further away than the first \n\n',...
    'Please press Y to start the task'];

% everyday instructions
everydayText = ['Welcome back! \n\n\n',...
    'In each condition you will receive different information about the objects''s \n\n',...
    'distance. Sometimes you will be able to see how far away the object is, \n\n',...
    'and sometimes you will hear a sounds that can help you tell how far away the \n\n',...
    'object is. There will be short breaks within each condition, and you can take \n\n',...
    'longer breaks between the conditions. You will not get feedback on each trial, \n\n',...
    'but you will be able to see your progress on each condition and through the task.. \n\n\n',...
    'Please press key A to indicate that the second object was closer than the first \n\n',...
    'Please press key B to indicate that the second object was further away than the first \n\n',...
    'Please press Y to start the task'];

% to word instructions
wordText = ['Well done! You have finished today''s experiment! \n\n\n',...
    'Before you go, we have one more thing for you to do today. Once \n\n',...
    'you press Y, this program will close and a Word document will \n\n',...
    'open. Please type your response(s) to the question(s) that you \n\n',...
    'are presented with in the document. When you are finished, please \n\n',...
    'contact the experimenter. \n\n\n',...
    'Press Y to close the program.'];

% finished instructions
finishedText = ['Well done! You have finished today''s experiment! \n\n\n',...
    'Once you press Y, this program will close. Please \n\n',...
    'contact the experimenter to say you have finished. \n\n\n',...
    'Press Y to close the program.'];

%% ----- RUN THE EXPERIMENT -----

% ----- SET UP THIS SESSION -----

% task type label
if part.session == 1
    params.taskType = 'training';
elseif part.session == 2
    params.taskType = 'combination';
elseif part.session == 3
    params.taskType = 'fusion';
elseif part.session == 4
    params.taskType = 'noisy_combination';
end

% define the disparity jitter
if part.session < 4
     params.sdJit = 0;
else
     params.sdJit = 1;
end

% define the number of each type of trial
if part.demo
    params.progressInt = 5;
    params.easyInt = 4;
    if part.session == 1
        params.maxTrials = 20;
    else
        params.maxTrials = 10;
    end
else
    params.progressInt = 75;
    params.easyInt = 20;
    if part.session == 1
        params.maxTrials = 300;
    else
        params.maxTrials = 150;
    end
end

% ----- RUN THE PRE-ASSESMENT OR LOAD GROUP AND INTEROCULAR INFO -----

if part.session == 1
    textBlock(params,preText);
    part = runAssesment(params,part);
else
    load([params.parentFolder,'data\',part.pID,'\',part.pID,'_','group.mat'],...
        'group','interocular','mapDir');
    part.group = group;
    part.interocularDistance = interocular;
    part.mapDir = mapDir;
end

% ----- RUN THE TASK -----

% only run the training task if they sucessfully passed the
% assesment
if ~strcmp(part.group,'None')
    if part.session == 1
        textBlock(params,trainText);
    else
        textBlock(params,everydayText);
    end
    [data,params] = runStaircases(params,part);
end
if ismember(params.taskType,{'combination','noisy_combination'})
    [interleavedData,params] = runInterleavedStaircases(params,part);
end

%% ----- END AND TIDY UP -----

% dump everything (just incase)
save(['memoryDumps\',params.taskType,'_Dump_',part.pID,'_',datestr(now,'dd-mmm-yyyy-HH-MM-SS')]);

% get rid of temp memory dumps (in a try catch as can be iffy)
try
    rmdir([params.parentFolder,'memoryDumps\temp',params.compID{1,1}],'s')
catch

end

%% ----- SAY THANKS OR OPEN WORD -----

if part.session == 2 || part.session == 4
    textBlock(params,wordText);
    openWordFile(params,part);
else
    textBlock(params,finishedText);
end
PsychPortAudio('Close');
sca;
exit




















