clc; clear all; close all;

% parent folder
parentFolder = 'C:\Users\User\Dropbox\ERC_QUEST\';

% add path to dependencies and functions
addpath(genpath([parentFolder,'dependencies']))
addpath(genpath([parentFolder,'functions']))

% sensory noise values to simulate power for
best = logspace(-1.3,-0.5,5);

% sensory noise ratios
ratios = logspace(0,0.1,5);

% Ns to simulate
N = 10:5:50;

% number of sims per parameter set
nSims = 1e3;

% get quest parameters
q = getQuestPlusParams;

% initialise with uniform priors (possibly from saved file to save
% time)
q.priors = {ones(1,length(q.paramDomain{1}))/length(q.paramDomain{1}), ...
    ones(1,length(q.paramDomain{2}))/length(q.paramDomain{2}), ...
    ones(1,length(q.paramDomain{3}))/length(q.paramDomain{3}), ...
    ones(1,length(q.paramDomain{4}))/length(q.paramDomain{4})};
q.fn = [parentFolder,'functions\QuestPlusLikelihoodsExp1.mat'];

%% Power analysis for detecting a combination effect

% where to save the power
combinationPower = nan(length(N),length(ratios),length(best));

% loop through the simulations
for i = 1:length(N)

    thisN = N(1,i);
    
    for j = 1:length(ratios)
    
        thisRatio = ratios(1,j);

        for k = 1:length(best)

            bestSig = best(1,k);
            worstSig = bestSig*ratios(1,j);
            bothSig = ((bestSig^2*worstSig^2)/(bestSig^2+worstSig^2))^(0.5);

            % where to save these sims
            theseSims = nan(nSims,1);

            for s = 1:nSims

                % where to save each n in this group
                theseEstimates = nan(thisN,3);

                for n = 1:thisN

                    % get a random lapse rate
                    LR = rand*0.14 + 0.01;
                    tic
                    for c = 1:3

                        % set up a new QUEST+ object
                        QP = QuestPlus(q.F, q.stimDomain, q.paramDomain, q.respDomain, ...
                            q.stopRule, q.stopCriterion);

                        % initialise with uniform priors (possibly from saved file to save
                        % time)
                        if exist(q.fn, 'file')
                            QP.initialise(q.priors, q.fn);
                        else
                            QP.initialise(q.priors);
                            QP.saveLikelihoods(q.fn);
                        end

                        if c == 1
                            % simulate best cue staircase
                            theseEstimates(n,1) = simulateCombinationStaircase(QP,bestSig,LR);
                        elseif c == 2
                            % simulate worst cue staircase
                            theseEstimates(n,2) = simulateCombinationStaircase(QP,worstSig,LR);
                        else
                            % simulate both cues staircase
                            theseEstimates(n,3) = simulateCombinationStaircase(QP,bothSig,LR);
                        end

                    end
                    toc

                end

                % do the test
                p = signrank(min(theseEstimates(n,1:2),[],2),theseEstimates(n,3),'tail','right');

                % save the result
                theseSims(s,1) = p < .05;

            end

            combinationPower(i,j,k) = (sum(theseSims)/nSims)*100;
            disp(['i = ',num2str(i),', j = ',num2str(j),', k = ',num2str(k),' done!'])

        end

    end

end

% plot the results
figure;
for p = 1:length(best)
   subplot(3,length(best),p); hold on;
   for r = 1:length(ratios)
       plot(N,combinationPower(:,r,p),'-','linewidth',1.5)     
   end
   set(gca,'fontsize',10)
   set(gca,'linewidth',1.5)
   title(['Best \sigma = ',num2str(round(best(1,p),2))])
   if p == 1
       ylabel({'COMBINATION';'Power'})
   end
end

%% Power analysis for detecting a re-weighting effect









%% Power analysis for detecting a fusion effect