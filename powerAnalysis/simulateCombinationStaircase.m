function recoveredSD = simulateCombinationStaircase(QP,sig,LR)

% what will the reference value be?
reference = 0;

% estimate parameters using QUEST+
tCounter = 0;
while ~QP.isFinished()

    % count trials
    tCounter = tCounter + 1;

    % get next test stimulus
    test = QP.getTargetStim();

    % simulate response
    x_r = randn .* sig + reference;
    x_t = randn .* sig + test;
    lapse = rand < LR;
    chooseTest = (1-lapse) .* (x_t > x_r) + lapse .* (randi(2)-1);

    % update QUEST+
    QP.update(test, chooseTest);

end

% get parameter estimates
fitParams = QP.getParamEsts('mean');

% variance of fitted cumulative Gaussian
fitSD = fitParams(2);
fitVar = fitSD^2;
recoveredSD = sqrt(fitVar/2);

end