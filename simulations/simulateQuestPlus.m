% Simulates responses from an observer with Gaussian sensory noise
% performing a 2AFC/IFC task and compares sensory noise recovered from
% QuestPlus (Jones 2018) to true values.
% Code does this for different lapse rates, and different levels of noise

clear; clc; close all;

%% ----- DEPENDENCIES -----

% path to parent folder
% parentFolder = 'D:\Dropbox\ERC_QUEST\';
parentFolder = 'C:\Users\User\Dropbox\ERC_QUEST\';

% add path to QuestPlus code
addpath(genpath([parentFolder,'dependencies\QuestPlus']))

%% ----- SIMULATION PARAMETERS -----

% the model
F = @(x,mu,sigma,gamma,lambda) gamma + (1 - gamma - lambda) .* normcdf(x,mu,sigma);

% parameter domains
mu = -0.1:0.025:0.1;
sigma = 0.01:0.01:1.25;
gamma = 0;
lambda = 0.01:0.01:0.15;
paramDomain = {mu, sigma, gamma, lambda};

% stimulus domain
stimDomain = -1:0.05:1;

% response domain
respDomain = [0 1]; % closer/further

% set stopping rule and criterion
stopRule = 'ntrials';
stopCriterion = 200;
% minNTrials = 10;
% maxNTrials = 500;

% true values to simulate responses for
sigmas = rand(50,1)*0.75 + 0.25;
lapseRates = [0.01 0.05 0.15];

% what will the reference value be?
reference = 0;

% plots
plots = 0;

%% ----- SIMULATE RESPONSES AND ESTIMATE NOISE -----

% where to save the estimated values
estimated = nan(length(sigmas),length(lapseRates));

% loop over each simulation
for l = 1:length(lapseRates)
    
    % this lapse rate
    LR = lapseRates(1,l);
    
    for s = 1:length(sigmas)
        
        % this sigma
        thisSig = sigmas(s,1);
        
        % create new QUEST+ object
        QP = QuestPlus(F, stimDomain, paramDomain, respDomain, ...
            stopRule, stopCriterion);
        
        % initialise with uniform priors (possibly from saved file to save
        % time)
        priors = {ones(1,length(paramDomain{1}))/length(paramDomain{1}), ...
            ones(1,length(paramDomain{2}))/length(paramDomain{2}), ...
            ones(1,length(paramDomain{3}))/length(paramDomain{3}), ...
            ones(1,length(paramDomain{4}))/length(paramDomain{4})};
        fn = 'myLikelihoods.mat';
        if exist(fn, 'file')
            QP.initialise(priors, fn);
        else
            QP.initialise(priors);
            QP.saveLikelihoods(fn);
        end
        
        % estimate parameters using QUEST+
        tCounter = 0;
        while ~QP.isFinished()
            
            % count trials
            tCounter = tCounter + 1;
            
            % get next test stimulus
            test = QP.getTargetStim();
            
            % simulate response
            x_r = randn .* thisSig + reference;
            x_t = randn .* thisSig + test;
            lapse = rand < LR;
            chooseTest = (1-lapse) .* (x_t > x_r) + lapse .* (randi(2)-1);
            
            % update QUEST+
            QP.update(test, chooseTest);
            
        end
        
        % get parameter estimates
        fitParams = QP.getParamEsts('mean');
        
        % variance of fitted cumulative Gaussian
        fitSD = fitParams(2);
        fitVar = fitSD^2;
        recoveredSD = sqrt(fitVar/2);
        
        % save the estimated noise
        estimated(s,l) = recoveredSD;
        
        % output
        disp([num2str(tCounter),' trials, ',...
            num2str(round(thisSig,2)),' vs. ',...
            num2str(recoveredSD)])

        % do a plot?
        if plots
            figure; hold on;
            plot(stimDomain,F(stimDomain,0,sqrt(2*thisSig^2),0,LR),'g','linewidth',1.5);
            plot(stimDomain,F(stimDomain,fitParams(1),fitParams(2),0,fitParams(3)),'b','linewidth',1.5);
        end
        
    end
    
    % note where we are up to
    disp(['l = ',num2str(l),' done!'])
    
end

%% ----- PLOT FIGURES AND GET CORRELATION AND MSE -----

rs = nan(length(lapseRates),1);
mse = nan(length(lapseRates),1);

figure;
for l = 1:length(lapseRates)
    subplot(1,length(lapseRates),l);
    hold on;
    plot([0 1],[0 1],'--k')
    plot(sigmas,estimated(:,l),'ok','linewidth',1.5,...
        'markersize',5)
    rs(l,1) = corr(sigmas,estimated(:,l));
    mse(l,1) = mean((sigmas-estimated(:,l)).^2);
    title({['Lapse Rate = ',num2str(lapseRates(1,l))];
        ['r = ',num2str(round(rs(l,1),3)),...
        '; MSE = ',num2str(round(mse(l,1),4))]})
    if l == 1
        ylabel('Estimated \sigma')
    end
    xlabel('True \sigma')
    set(gca,'fontsize',12)
    set(gca,'linewidth',1.5)
end

figure; hold on;
bar(lapseRates,rs,'k')
title('Correlations: estimated vs. true \sigma')
ylabel('Correlation')
xlabel('Lapse Rate')
set(gca,'fontsize',12)
set(gca,'linewidth',1.5)
ylim([0,1])

figure; hold on;
bar(lapseRates,mse,'k')
title('MSE: estimated - true \sigma')
ylabel('MSE')
xlabel('Lapse Rate')
set(gca,'fontsize',12)
set(gca,'linewidth',1.5)
%ylim([0,0.01])














