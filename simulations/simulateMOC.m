% Simulates responses from an observer with Gaussian sensory noise
% performing a 2AFC/IFC task and compares sensory noise recovered from
% cumulative Gaussian fit using Palamedes code to true values.
% Code does this for different stimulus step sizes, different numbers of
% trials per point, different lapse rates, and different levels of noise

clear; clc; close all;

%% ----- DEPENDENCIES -----

% path to parent folder
parentFolder = 'D:\Dropbox\ERC_QUEST\';

% add path to Palamedes code
addpath(genpath('dependencies\Palamedes1_8_2'))

%% ----- SIMULATION PARAMETERS -----

stimulusRange = [-1,1];
reference = 0;
noise = rand(50,1)*0.5 + 0.1;
nStimuli = 6:4:18;
lapseRates = [0.01 0.05 0.1];
nReps = 10:5:20;
plots = 0;

%% ----- SIMULATE RESPONSES AND ESTIMATE NOISE -----

% where to save the estimated values
estimated = nan(length(noise),length(nStimuli),length(lapseRates),length(nReps));

% loop over each simulation and do the fit
for n = 1:length(nReps)
    N = nReps(n);
    for l = 1:length(lapseRates) 
        LR = lapseRates(1,l);
        for s = 1:length(nStimuli)
            tests = repmat(linspace(stimulusRange(1,1),stimulusRange(1,2),nStimuli(1,s))',[N,1]);
            for sig = 1:length(noise)
                % simulate responses
                sigma = noise(sig,1);
                x_r = randn(length(tests),1) .* sigma + reference;
                x_t = randn(length(tests),1) .* sigma + tests;
                lapse = rand(length(tests),1) < LR;
                chooseTest = (1-lapse) .* (x_t > x_r) + lapse .* (randi(2,[length(tests),1])-1);
                % specify initial parameters for the fit
                thresholdGuess = 0;
                slopeGuess = 3;
                guessRate = 0;
                lapseGuess = 0.01;
                % specify max lapse rate
                maxLapse = 0.2;
                % prep data for fitting
                uniqueTests = unique(tests);
                counts = nan(1,length(uniqueTests));
                for t = 1:length(uniqueTests)
                    counts(1,t) = sum(chooseTest(round(tests,3) == round(uniqueTests(t,1),3)));
                end
                % fit cumulative Gaussian
                [fitParams,LL,exitflag,output] = fitCumulativeGaussian(...
                    uniqueTests',counts,ones(1,length(uniqueTests)).*N,thresholdGuess,slopeGuess,...
                    guessRate,lapseGuess,maxLapse);
                % plots?
                if plots
                    figure; hold on;
                    plot(uniqueTests,counts./(ones(1,length(uniqueTests)).*N),'xk')
                    plot(uniqueTests,PAL_CumulativeNormal(fitParams,uniqueTests),'-b')
                end
                % variance of fitted cumulative Gaussian
                fitSD = 1/fitParams(1,2);
                fitVar = fitSD^2;
                recoveredSD = sqrt(fitVar/2);
                % save the estimated noise
                estimated(sig,s,l,n) = recoveredSD;
            end
            % note where we are up to
            disp(['n = ',num2str(n),...
                ', l = ',num2str(l),...
                ', s = ',num2str(s),...
                ' done!'])
        end
    end
end

%% ----- PLOT FIGURES AND GET CORRELATION AND MSE -----

rs = nan(length(nStimuli),length(lapseRates),length(nReps));
mse = nan(length(nStimuli),length(lapseRates),length(nReps));

for n = 1:length(nReps)
    figure;
    for s = 1:length(nStimuli)
        for l = 1:length(lapseRates)
            subplot(length(lapseRates),length(nStimuli),s+(l-1)*length(nStimuli));
            hold on;
            plot([0 1],[0 1],'--k')
            plot(noise,estimated(:,s,l,n),'ok','linewidth',1.5,...
                'markersize',5)
            rs(s,l,n) = corr(noise,estimated(:,s,l,n));
            mse(s,l,n) = mean((noise-estimated(:,s,l,n)).^2);
            if s == 1 && l == 1
                title({[num2str(nReps(n)),' repeats for each stimulus'];...
                    ['r = ',num2str(round(rs(s,l,n),3)),...
                    '; MSE = ',num2str(round(mse(s,l,n),4))]})
            else
                title(['r = ',num2str(round(rs(s,l,n),3)),...
                    '; MSE = ',num2str(round(mse(s,l,n),4))])
            end
            if s == 1
                ylabel({['Lapse Rate = ',num2str(lapseRates(1,l))];'Estimated \sigma'})
            end
            if l == length(lapseRates)
                xlabel({'True \sigma',['Number of Stimuli = ',num2str(nStimuli(1,s))]})
            end
            set(gca,'fontsize',12)
            set(gca,'linewidth',1.5)
        end
    end
end

figure;
for s = 1:length(nStimuli)
    for l = 1:length(lapseRates)
        subplot(length(lapseRates),length(nStimuli),s+(l-1)*length(nStimuli));
        hold on;
        bar(nReps,squeeze(rs(s,l,:)),'k')
        if s == 1 && l == 1
            title('Correlations: estimated vs. true \sigma')
        end
        if s == 1
            ylabel({['Lapse Rate = ',num2str(lapseRates(1,l))];'Correlation'})
        end
        if l == length(lapseRates)
            xlabel(['Number of Stimuli = ',num2str(nStimuli(1,s))])
        end
        set(gca,'fontsize',12)
        set(gca,'linewidth',1.5)
        ylim([0,1])
    end
end

figure;
for s = 1:length(nStimuli)
    for l = 1:length(lapseRates)
        subplot(length(lapseRates),length(nStimuli),s+(l-1)*length(nStimuli));
        hold on;
        bar(nReps,squeeze(mse(s,l,:)),'k')
        if s == 1 && l == 1
            title('MSE: estimated - true \sigma')
        end
        if s == 1
            ylabel({['Lapse Rate = ',num2str(lapseRates(1,l))];'MSE'})
        end
        if l == length(lapseRates)
            xlabel(['Number of Stimuli = ',num2str(nStimuli(1,s))])
        end
        set(gca,'fontsize',12)
        set(gca,'linewidth',1.5)
        ylim([0,0.03])
    end
end














