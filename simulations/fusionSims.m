clear; clc; close all;

%% ----- DEPENDENCIES -----

% path to parent folder
% parentFolder = 'D:\Dropbox\ERC_QUEST\';
% parentFolder = 'C:\Users\User\Dropbox\ERC_QUEST\';
parentFolder = 'C:\Users\stace\Dropbox\ERC_QUEST\';

% add path to QuestPlus code
addpath(genpath([parentFolder,'dependencies\QuestPlus']))

%% ----- ILLUSTRATIVE PLOTS -----

% relationship between JND_i - JND_c and w_1
figure; hold on;
ratios = linspace(1e-2,1,1e2);
ratios = [ratios,1./ratios];
w = ratios./(1+ratios);
JND_c = sqrt(2*w);
JND_i = abs(1./(2*w-1)).*JND_c;
diff = JND_i - JND_c;
plot([0.5 0.5],[0,100],'--k')
plot(w,diff,'-k','linewidth',1.7)
ylabel({'Increase in JND under incongruence';'JND_i - JND_c'})
xlabel('Weight on cue 1')
set(gca,'fontsize',12)
set(gca,'linewidth',1.5)
ylim([0,100])

% relationship between var_b - var_c and w_1
figure; hold on;
var_b = min([ones(1,length(w));ratios]);
var_c = w;
diff_c = var_b - var_c;
plot(w,diff_c,'-k','linewidth',1.7)
ylabel({'Gain in precision';'var_b - var_c'})
xlabel('Weight on cue 1')
set(gca,'fontsize',12)
set(gca,'linewidth',1.5) 

%% ----- SIMULATION PARAMETERS -----

% the model
F = @(x,mu,sigma,gamma,lambda) gamma + (1 - gamma - lambda) .* normcdf(x,mu,sigma);

% parameter domains
mu = -0.1:0.025:0.1;
sigma = 0.01:0.01:1.25;
gamma = 0;
lambda = 0.01:0.01:0.15;
paramDomain = {mu, sigma, gamma, lambda};

% stimulus domain
stimDomain = -1:0.05:1;

% response domain
respDomain = [0 1]; % closer/further

% set stopping rule and criterion
stopRule = 'ntrials';
stopCriterion = 200;

% how many simulations
nSims = 50;

% what will the reference value be?
reference = 0;

% plots
plots = 0;

%% ----- SIMULATE RESPONSES AND ESTIMATE NOISE -----

% where to save the estimated values
estimated = nan(nSims,4);
    
for s = 1:nSims
    
    % get a lapse rate
    LR = rand*0.09 + 0.01;
    
    % this sigma
    sig1 = rand*0.4 + 0.1;
    sig2 = rand*0.4 + 0.1;
    sigb = (sig1^2 * sig2^2)/(sig1^2 + sig2^2);
    w1 = sig2^2/(sig1^2 + sig2^2);
    
    % loop over condition (cue 1 only, cue 2 only, congruent, incongruent)
    for c = 1:4
        
        % create new QUEST+ object
        QP = QuestPlus(F, stimDomain, paramDomain, respDomain, ...
            stopRule, stopCriterion);
        
        % initialise with uniform priors (possibly from saved file to save
        % time)
        priors = {ones(1,length(paramDomain{1}))/length(paramDomain{1}), ...
            ones(1,length(paramDomain{2}))/length(paramDomain{2}), ...
            ones(1,length(paramDomain{3}))/length(paramDomain{3}), ...
            ones(1,length(paramDomain{4}))/length(paramDomain{4})};
        fn = 'myLikelihoods.mat';
        if exist(fn, 'file')
            QP.initialise(priors, fn);
        else
            QP.initialise(priors);
            QP.saveLikelihoods(fn);
        end
        
        % estimate parameters using QUEST+
        tCounter = 0;
        while ~QP.isFinished()
            
            % count trials
            tCounter = tCounter + 1;
            
            % get next test stimulus
            test = QP.getTargetStim();
            
            % simulate response
            if c == 1 % cue 1 only
                x_r = randn .* sig1 + reference;
                x_t = randn .* sig1 + test;
            elseif c == 2 % cue 2 only
                x_r = randn .* sig2 + reference;
                x_t = randn .* sig2 + test;
            elseif c == 3 % congruent cues
                x_r = randn .* sigb + reference;
                x_t = randn .* sigb + test;
            else % incongruent cues
                x_r = randn .* sigb + reference;
                x_t = randn .* sigb + (2*w1-1)*test;
            end
            lapse = rand < LR;
            chooseTest = (1-lapse) .* (x_t > x_r) + lapse .* (randi(2)-1);
            
            % update QUEST+
            QP.update(test, chooseTest);
            
        end
        
        % get parameter estimates
        fitParams = QP.getParamEsts('mean');
        
        % variance of fitted cumulative Gaussian
        fitSD = fitParams(2);
        fitVar = fitSD^2;
        recoveredSD = sqrt(fitVar/2);
        
        % save the estimated noise
        estimated(s,c) = recoveredSD;
        
        % do a plot?
        if plots
            if c == 1
                figure; hold on;
            end
            plot(stimDomain,F(stimDomain,fitParams(1),fitParams(2),0,fitParams(3)),'linewidth',1.5);
            if c == 4
               ylabel('P(Test Further)')
               xlabel('Test-Reference')
               legend('Cue 1','Cue 2','Congruent','Incongruent')
            end
        end
        
    end
    
    % note where we are up to
    disp(['s = ',num2str(s),' done!'])
    
end

estimated = (2.*(estimated.^2)).^0.5;

figure; hold on;
boxplot(estimated);











