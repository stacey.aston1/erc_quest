function [fitParams,LL,exitflag,output] = fitCumulativeGaussian(...
    stims,responseCount,N,thresholdGuess,slopeGuess,...
    guessRate,lapseGuess,maxLapse)

PF = @PAL_CumulativeNormal; % specify the function we are fitting

initParams = [thresholdGuess slopeGuess guessRate lapseGuess]; % specify the parameters (alpha,beta,gamma,lambda)

paramsFree = [1 1 0 1]; % which parameters are free i.e. are being estimated (1 = free, 0 = fixed)

options = PAL_minimize('options');
options.TolX = 1e-20;
options.TolFun = 1e-20;
options.MaxIter = 1000*length(initParams);
options.MaxFunEvals = 1000*length(initParams);

[fitParams,LL,exitflag,output] = PAL_PFML_Fit(...
    stims,responseCount,N,...
    initParams,paramsFree,PF,...
    'lapseLimits',[0 maxLapse],...
    'searchOptions',options);
% exitflag tells us if the fit was successful
% paramsValues are fitted parameters
% LL is the value of the log likelihood

end