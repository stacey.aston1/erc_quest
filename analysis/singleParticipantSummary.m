function [] = singleParticipantSummary(parentFolder,pID,day)

% the model
F = @(x,mu,sigma,gamma,lambda) gamma + (1 - gamma - lambda) .* normcdf(x,mu,sigma);

if day == 1
    conditions = {'D','S','SD','AD'};
    taskType = 'training';
elseif day == 2
    conditions = {'D','S','SD','AD','A'};
    taskType = 'combination';
    if pID == 3
        rwConditions = {'SD+-'};
    else
        rwConditions = {'SD+-','AD+-'};
    end
else
    error('Other days not implemented yet') 
end

% stimulus domain
stims = -1:0.1:1;

% do combination plots
if day == 1
    figure('position',[100 100 1700 800]);
else
    figure('position',[0 100 1920 900]);
end
for c = 1:length(conditions) 
    
    % load this data
    T = readtable([parentFolder,'data\ERC_Exp1_',num2str(pID),'\ERC_Exp1_',...
        num2str(pID),'_QUEST_',taskType,'_',conditions{1,c},'_trials.csv']);
    
    % load these fit params
    M = readtable([parentFolder,'data\ERC_Exp1_',num2str(pID),'\ERC_Exp1_',...
        num2str(pID),'_',taskType,'_',conditions{1,c},'_measures.csv']);
    
    % convert to function parameters
    mu = M{1,'PSE'};
    sigma = sqrt(2*(M{1,'sensory_noise'}^2));
    gamma = 0;
    lambda = M{1,'lapse_rate'};
    
    % find how many times each stimulus was shown and the percent further
    count = nan(1,length(stims));
    percent = nan(1,length(stims));
    for s = 1:length(stims)
        inds = find(round(T{:,'test'},1) == round(stims(1,s),1));
        count(1,s) = length(inds);
        if count(1,s)
            percent(1,s) = sum(strcmp(T{inds,'response'},'further'))/count(1,s);
        end
    end
    
    % plot percent further and overlay function
    subplot(2,length(conditions),c); hold on;
    plot(stims,percent,'ok','markersize',5,'linewidth',1.5)
    plot(stims,F(stims,mu,sigma,gamma,lambda),'-b','linewidth',1.7)
    text(0.6,0.1,['\sigma = ',num2str(round(M{1,'sensory_noise'},2))])
    set(gca,'fontsize',12)
    set(gca,'linewidth',1.5)
    ylabel('Percent further')
    title({['Part ',num2str(pID)];[taskType,': ',conditions{1,c}]})
    
    % plot a histogram
    subplot(2,length(conditions),c+length(conditions)); hold on;
    bar(stims,count,'k')
    set(gca,'fontsize',12)
    set(gca,'linewidth',1.5)
    ylabel('Number of times shown')
    xlabel('Test value')
  
end

% save the image
saveas(gcf,[parentFolder,'\analysis\figures\singlePs\ERC_Exp1_',num2str(pID),'_',taskType,'.png'])

% do re-weighting plots if day 2 or 4
if day == 2 || day == 4
    figure('position',[300 200 1400 400]);
    colours = {'-b','--r',':g'};
    markers = {'ob','sr','^g'};
    for c = 1:length(rwConditions)
        
        % get the predicted shift
        DT = readtable([parentFolder,'data\ERC_Exp1_',num2str(pID),'\ERC_Exp1_',...
            num2str(pID),'_',taskType,'_D_measures.csv']);
        OT = readtable([parentFolder,'data\ERC_Exp1_',num2str(pID),'\ERC_Exp1_',...
            num2str(pID),'_',taskType,'_',rwConditions{1,c}(1),'_measures.csv']);
        w = DT{1,'sensory_noise'}^2/(DT{1,'sensory_noise'}^2 + OT{1,'sensory_noise'}^2);
        delta = 1.5*OT{1,'JND'};
        expectedShift = w*delta;
        
        % load the non-conflict data for this condition first and plot then
        % put others on top
        for f = 1:3
            
            switch f
                case 1
                    trialID = rwConditions{1,c}(1:2);
                    measuresID = rwConditions{1,c}(1:2);
                case 2
                    trialID = [rwConditions{1,c},'_neg'];
                    measuresID = rwConditions{1,c};
                case 3
                    trialID = [rwConditions{1,c},'_pos'];
                    measuresID = rwConditions{1,c};
            end
            
            % load the data
            T = readtable([parentFolder,'data\ERC_Exp1_',num2str(pID),'\ERC_Exp1_',...
                num2str(pID),'_QUEST_',taskType,'_',trialID,'_trials.csv']);
            
            % load the fit params
            M = readtable([parentFolder,'data\ERC_Exp1_',num2str(pID),'\ERC_Exp1_',...
                num2str(pID),'_',taskType,'_',measuresID,'_measures.csv']);
            
            % convert to function parameters
            if f == 2
                ind = 2;
            else
                ind = 1;
            end
            mu = M{ind,'PSE'};
            sigma = sqrt(2*(M{ind,'sensory_noise'}^2));
            gamma = 0;
            lambda = M{ind,'lapse_rate'};
            
            % find how many times each stimulus was shown and the percent further
            count = nan(1,length(stims));
            percent = nan(1,length(stims));
            for s = 1:length(stims)
                inds = find(round(T{:,'test'},1) == round(stims(1,s),1));
                count(1,s) = length(inds);
                if count(1,s)
                    percent(1,s) = sum(strcmp(T{inds,'response'},'further'))/count(1,s);
                end
            end
            
            % plot percent further and overlay function
            subplot(length(rwConditions),3,1+(c-1)*length(rwConditions)); hold on;
            plot(stims,percent,markers{1,f},'markersize',5,'linewidth',1.5)
            h(f) = plot(stims,F(stims,mu,sigma,gamma,lambda),colours{f},'linewidth',1.7);
            if f == 3
                text(-0.9,0.8,['optimal shift = ',num2str(round(expectedShift,2))])
                set(gca,'fontsize',12)
                set(gca,'linewidth',1.5)
                xlabel('Test value')
                ylabel({['Part ',num2str(pID)];['Re-weighting: ',rwConditions{1,c}(1:2)];'Percent further'})
                legend(h,{'No conflict','-\delta','+\delta'},'location','southeast')
            end
            
            % plot a histogram
            if f > 1
                subplot(length(rwConditions),3,f+(c-1)*length(rwConditions)); hold on;
                bar(stims,count,'k')
                set(gca,'fontsize',12)
                set(gca,'linewidth',1.5)
                ylabel('Number of times shown')
                xlabel('Test value')
                if f == 2
                    title('Negative Conflict')
                else
                    title('Positive Conflict')
                end
            end
            
        end
        
    end
    
    % save the image
    saveas(gcf,[parentFolder,'\analysis\figures\singlePs\ERC_Exp1_',num2str(pID),...
        '_reweighting_',rwConditions{1,c}(1:2),'.png'])
    
end

end





